const moment = require("moment-timezone")
const fs     = require("fs")

const allTimezones = moment.tz.names() //list of all tz.db timezones.

let config //global variable for the user timezone config.
fs.readFile("./config.json", "utf-8", (err, data) => {
  if(err) throw err
  config = JSON.parse(data)
})

module.exports = (bot, cmd, user) => {

  //&time (default output)
  if(!cmd[1]) {
    return "Current Server Time: **" + moment().tz("UTC").format("Do MMMM YYYY HH:mm") + "**."
  }

  //&time me <timezone>
  if(cmd[1] == "me") {
    return addUserToConfig(bot, user, cmd[2])
  }

  //&time check <@username>
  if(cmd[1] == "check") {
    let username = cmd
    username.splice(0, 2)
    username = username.join(" ")
    return checkUsersTimeZone(bot, user, username)
  }

  //&time convert <time> <timezone> <day>
  if(cmd[1] == "convert") {
    return convertTime(bot, cmd[2], cmd[3], cmd[4])
  }

  return "Unhandled command arguments. Verify the command input."
}

function addUserToConfig(bot, user, timezone) {

  //bot - the bot object from discord.js
  //user - command sender's user object
  //timezone - selected timezone
  let id = user.id
  let name = user.username

  //check if timezone exists
  let zone
  for(el of allTimezones) {
    if(el===timezone) {
      zone = el
      break
    }
  }
  if(!zone) {
    //If the selected timezone doesn't exist.
    return `Timezone **${timezone}** not found. Use the format **Continent/City** (e.g. **Europe/London**) or the timezone shorthand (e.g. **CET**)`
  }

  //Add user to config and save the config.
  config[id] = timezone
  fs.writeFile("./config.json", JSON.stringify(config), (err) => {
    if(err) throw err
  })

  //Output messages.
  if(config[id]) return `Updated user **${name}** with timezone **${timezone}**.`
  else return `Added user **${name}** with timezone **${timezone}**.`

}

function checkUsersTimeZone(bot, user, target) {
  let id = bot.users.find("username", target)
  if(!id) return `User **${target}** not found.`
  else id = id.id

  let timezone = config[id]
  if(!timezone) return `The user has not declared their timezone.`

  let time = moment().tz(timezone).format("Do MMMM YYYY HH:mm")
  return `The current time in the timezone **${timezone}** for user ${bot.users.get(id).username} is: **${time}**.`
}

function convertTime(bot, timestamp, timezone, day) {
  let time
  if(timestamp=="now") {
    time = moment()
  }
  else {
    time = moment(timestamp, "HH:mm")
  }

  if(day) {
    if(day=="tomorrow") day=1
    if(day=="yesterday") day=-1
    if(day=="today") day=0

    time.add(day, "days")
  }


  let zone
  for(el of allTimezones) {
    if(el===timezone) {
      zone = el
      break
    }
  }

  if(!zone) {
    return `Timezone **${timezone}** not found. Use the format **Continent/City** (e.g. **Europe/London**) or the timezone shorthand (e.g. **CET**)`
  }

  let converted = time.clone()
  converted.tz(timezone)

  return `Time **${time.format("Do MMMM YYYY HH:mm")}**. Converted time **${converted.format("Do MMMM YYYY HH:mm")}** in timezone **${timezone}**.`

  return time
}
