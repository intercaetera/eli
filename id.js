const request = require('request')

const API = "http://85.255.12.57/stardust/ids"

module.exports = (bot, cmd, message) => {
  if(!cmd[1]) {
    message.channel.sendMessage("Specify an argument.")
  }

  if(cmd[1] == "search") {
    if(cmd[2] && cmd[3]) {
      let param = cmd[2]
      let value = cmd
      value.splice(0, 3)
      value = value.join(" ")

      request(`${API}/search/${param}/${value}`, (err, res, body) => {
        if(err) throw err
        body = JSON.parse(body)

        let names = []
        for(each of body) {
          let element = `${each.name} (*${each.nick}*)`
          names.push(element)
        }

        names = names.join("\n")

        let output =
`${param} : ${value}:
Factions with **${param}** containing **${value}** are:

${names}
`
        message.channel.sendMessage(output, {split: true}).catch(console.error)

      })
    }
  }

  if(cmd[1] == "nick") {
    let nick = cmd[2]

    request(API+"/nick/"+nick, (err, res, body) => {
      if(err) throw err
      let id = JSON.parse(body)

      let type=""
      if(id.type == "lawful") type="Lawful ID"
      else if(id.type == "unlawful") type="Unlawful ID"
      else if(id.type == "quasi-lawful") type="Quasi-Lawful ID"

      let lines=""
      for(el of id.lines) {
        lines+="• "+el+"\n"
      }

      let ships = id.ships.join(", ")

      let output =
`${nick}:
__**${id.name} ID**__ - ${type}
${id.desc}

${lines}
**Zone of Influence:** ${id.zoi}
**Permitted ships:** ${ships}`

      message.channel.sendMessage(output)

    })
  }
}
