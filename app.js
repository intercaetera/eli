const Discord = require("discord.js")

const secrets = require("./secrets.js")
const time = require("./time.js")
const help = require("./help.js")
const id = require("./id.js")
const utility = require("./utility.js")

let bot = new Discord.Client()

bot.on("ready", () => {
  console.log("Ready!");
})

bot.on("message", (message) => {

  if(message.guild && message.channel) {
    console.log(message.channel.name + "@" + message.guild.name + " | " + message.author.username+": "+message.content)
  }
  else {
    console.log("PM" + " | " + message.author.username+": "+message.content)
  }

  const prefix = "&"

  if(!message.content.startsWith(prefix)) return

  //parse command
  let cmd = message.content.split(" ")
  cmd = cmd.filter((el) => {
    if(el != "") return true
  })
  cmd[0] = cmd[0].substr(1)

  if(cmd[0] == "ping") {
    message.channel.sendMessage("Pong.")
  }

  if(cmd[0] == "time") {
    message.channel.sendMessage(time(bot, cmd, message.author))
  }

  if(cmd[0] == "help") {
    help(bot, cmd, message)
  }

  if(cmd[0] == "id") {
    id(bot, cmd, message)
  }

  if(cmd[0] == "u" || cmd[0] == "utility") {
    utility(bot, cmd, message)
  }
})

bot.login(secrets.token)
