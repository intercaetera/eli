# Eli
-----
Eli is a Discovery Freelancer themed Discord bot.

### Installation:
Prerequisites: `git`, `nodejs`, `npm`  
Optionally: `nodemon`, `forever`

Create an application on your Discord developers page and fetch your bot token from it.
```
$ git clone http://gitlab.com/mrhudson/eli  
$ cd eli
$ npm install
```
Now you should edit the `secrets.js` config file with necessary information. 

### Running:
To run the bot you can simply use the command `node app.js`.  
If you want to run the bot with `nodemon` (restart on code change):
```
$ npm install --global nodemon
$ nodemon app.js -e js
```
If yo uwant to run the bot with `nodemon` and `forever` (restart on code change or crash, recommended on servers but that depends on your deployment mechanism):
```
$ npm install --global forever
$ forever start -c nodemon app.js -e js
```

#### Credits:
Made possible with discord.js. Made by Corile / MrHudson. MIT Licence.
